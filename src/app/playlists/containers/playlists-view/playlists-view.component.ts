import { Component, Input, OnInit } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-view',
  templateUrl: './playlists-view.component.html',
  styleUrls: ['./playlists-view.component.scss']
})
export class PlaylistsViewComponent implements OnInit {

  mode: 'details' | 'edit' | 'create' = 'details'

  playlists: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'my favourite playlist'
  }, {
    id: '234',
    name: 'Playlist 234',
    public: false,
    description: 'my favourite playlist'
  }, {
    id: '345',
    name: 'Playlist 34',
    public: true,
    description: 'my favourite playlist'
  }]

  selected?: Playlist

  selectPlaylist(playlist: Playlist) {
    this.selected = playlist
  }

  constructor() { }

  switchToEditMode() {
    this.mode = 'edit'
  }

  switchToCreateMode() {
    this.mode = 'create'
  }

  cancelEditing() {
    this.mode = 'details'
  }

  saveChanges(draft: Playlist) {
    if (!draft.id) {
      draft.id = (Date.now() + '')
      this.playlists.push(draft)
    } else {
      this.playlists = this.playlists.map(p => p.id == draft.id ? draft : p)
      this.playlists = this.playlists.map(p => p.id == draft.id ? draft : p)
    }
    this.selected = draft;
    this.mode = 'details'
  }

  deletePlaylistById(id: Playlist['id']) {
    this.playlists = this.playlists.filter(p => p.id == id)
    this.selected = this.selected?.id == id ? undefined : this.selected;
    this.mode = 'details'
  }

  ngOnInit(): void {
  }

}
