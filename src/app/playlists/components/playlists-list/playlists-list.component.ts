import { NgForOf, NgForOfContext } from '@angular/common';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Playlist } from '../../model/Playlist';

@Component({
  selector: 'app-playlists-list',
  templateUrl: './playlists-list.component.html',
  styleUrls: ['./playlists-list.component.scss'],
  // inputs:['playlists:items']
})
export class PlaylistsListComponent implements OnInit {

  @Input('items') playlists: Playlist[] = []

  @Input() selected?: Playlist

  @Output() selectedChange = new EventEmitter<Playlist>()

  @Output() remove = new EventEmitter<Playlist['id']>();


  removeClick(playlist: Playlist) {
    this.remove.emit(playlist.id)
  }

  select(playlist: Playlist) {
    // this.selected = playlist
    this.selectedChange.emit(playlist)
  }

  constructor() { }

  ngOnInit(): void {
  }

}
