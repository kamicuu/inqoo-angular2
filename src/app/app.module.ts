import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { CoreModule } from './core/core.module';

@NgModule({
  declarations: [ // componetns, directives, pipes..
    AppComponent,
  ],
  imports: [ // sub-modules
    BrowserModule,
    CoreModule,
    PlaylistsModule,
    AppRoutingModule,
  ],
  providers: [], // Services
  bootstrap: [AppComponent /* ,HeaderComp, FooterComp.. */]
})
export class AppModule /* implements DoBootstrap */ {

  // ngDoBootstrap(appRef: ApplicationRef): void {
  //   // fetch json confg then....
  //   appRef.bootstrap(AppComponent, 'app-root')
  // }
}


// console.log(AppModule)