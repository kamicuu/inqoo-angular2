import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { catchError, concatAll, exhaust, exhaustMap, filter, map, mergeAll, switchAll, switchMap, tap } from 'rxjs/operators';
import { Album } from 'src/app/core/model/album';
import { AlbumSearchService } from 'src/app/core/services/album-search/album-search.service';


/* 
  TODO:
  - load this on /music/albums?id=5Tby0U5VndHW0SomYO7Id7
  - show Id in html
  - fetch Alubm from server
  - show album Image, name
  - search results -> click card -> redirects here
*/

@Component({
  selector: 'app-album-details-view',
  templateUrl: './album-details-view.component.html',
  styleUrls: ['./album-details-view.component.scss']
})
export class AlbumDetailsViewComponent implements OnInit {

  results!: Album 
  message = '';
  query = '';
  SEARCH_PARAM = 'id'

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: AlbumSearchService
  ) { }

  ngOnInit(): void {

    this.route.queryParamMap.pipe(
      map(params => params.get(this.SEARCH_PARAM)),
      filter((q): q is string => q !== null),
      tap(query => this.query = query),
      switchMap(query => this.service.getAlbumById(query)),
    )
      .subscribe({
        next: (album) => this.results = album,
        error: (error) => this.message = error.message,
      })

  }

}
